/***************************************************************************************************************************************************
/* DataTableUtils */
/***************************************************************************************************************************************************/
class DataTableUtils {
    constructor() {}
    
    static async create(id, paging) {
        $(document).ready(function() { 
            $(id).DataTable({
                destroy: true,
                lengthChange: true,
                searching: true,                       
                stateSave: true,
                paging: paging,
                pagingType: "full_numbers",
                language: {
                    "sEmptyTable":   "",
                    "sLoadingRecords": "A carregar...",
                    "sProcessing":   "A processar...",
                    "sLengthMenu":   "Mostrar _MENU_ registros",
                    "sZeroRecords":  "",
                    "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
                    "sInfoPostFix":  "",
                    "sSearch":       "Buscar:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext":     "Seguinte",
                        "sLast":     "Último"
                    },
                    "oAria": {
                        "sSortAscending":  ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                } 
            }); 
        });
    }

    static async clear(id) {
        $(id).DataTable().clear();
    }
    
    static async addRow(id, row) {
        $(id).dataTable().fnAddData(row);
    }
    
    static async getRow(id, column, value) {
        var table = $(id).DataTable();
        
        var data = table.rows().data();
        
        for(var i = 0; i < data.length; i++) {
            var row_data = data[i];
            if(row_data[column] == value) return row_data;
        }
        
        return null;
    }
    
    static async redraw(id) {
        $(id).DataTable().draw();
    } 
    
    static formatStatus(status) {
        if (status === 1 || status === "1") { return "<span class=\"status-finalizado\">Finalizado</span>"; }
        if (status === 2 || status === "2") { return "<span class=\"status-pendente\">Pendente</span>"; }
        if (status === 3 || status === "3") { return "<span class=\"status-em-execucao\">Em execução</span>"; }  
        if (status === 4 || status === "4") { return "<span class=\"status-a-iniciar\">A iniciar</span>"; }
    }
    
    static formateStatusOfProcess(status) {
        if (status === 1 || status === "1") { return "<span class=\"status-a-iniciar\">Novo</span>"; }
        if (status === 2 || status === "2") { return "<span class=\"status-em-execucao\">Ativo</span>"; } 
        if (status === 3 || status === "3") { return "<span class=\"status-finalizado\">Finalizado</span>"; }
        if (status === 4 || status === "4") { return "<span class=\"status-arquivado\">Arquivado</span>"; }
    }
    
    static formateStatusOfTechnicalNote(status) {
        if (status === 1 || status === "1") { return "<span class=\"status-em-execucao\">Em elaboração</span>"; }
        if (status === 2 || status === "2") { return "<span class=\"status-em-execucao\">Em revisão</span>"; } 
        if (status === 3 || status === "3") { return "<span class=\"status-finalizado\">Concluído(a)</span>"; }
        if (status === 4 || status === "4") { return "<span class=\"status-a-iniciar\">Novo(a)</span>"; }
        if (status === 5 || status === "5") { return "<span class=\"status-em-execucao\">Correções</span>"; }
        if (status === 6 || status === "6") { return "<span class=\"status-em-execucao\">Corrigido(a)</span>"; }
        if (status === 7 || status === "7") { return "<span class=\"status-arquivado\">Arquivado(a)</span>"; }
    }

    
    static exportToCsv(header, name, data) {
        var csv = header + "\n";
        
        data.forEach(function(row){         
            csv += row.join(",");
            csv += "\n";
        });

        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=ISO-8859-1,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = name + ".csv";
        hiddenElement.click();
    }
    
    static align(text, align) {
        return "<span style='display:block;text-align:"+align+"'>" + text + "</span>";     
    }
    
    static limitText(text, limit) {
        if(text.length > limit) {
            return "<span title='"+text+"' style='display:block;text-align:left'>" + text.substring(0, limit) + "... </span>";
        } else {
            return "<span style='display:block;text-align:left'>" + text + "</span>";
        }
    }
    
    static tooltipText(text, tooltip) {
        return "<span title='"+tooltip+"' style='display:block;text-align:left'>" + text + "</span>";
    }
}



