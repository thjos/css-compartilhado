Array.prototype.getRowDataById = function(id) {
    var new_array = this.filter(function(row) {
        return row.id == id;
    });

    return new_array[0];
};

Array.prototype.deleteRowById = function(id) {
    var old_array = this;
    
    old_array.forEach(function(item, index, array) {
        if(item.id == id) {
            if(item.state != "I") {
                item.state = 'D';
            } else {
                array.splice(index, 1);
            }
        }     
    });
};

Array.prototype.updateRowData = function(new_row) {
    var inner_array = this;
    
    for(var i = 0; i < inner_array.length; i++) {
        if(inner_array[i].id == new_row.id) {
            if(new_row.state != "I" && new_row.state != "D") {
                new_row.state = 'U';
            }            
            inner_array[i] = new_row;
        }
    }
};

Array.prototype.getLastOrder = function() {
    var last_order = 1;  
    
    for(var i = 0; i < this.length; i++) {
       var register = this[i];                 
       if(register.ordem > last_order) {
           last_order = register.ordem;
       }
    } 
    
    return parseInt(last_order) + 1;
};

Array.prototype.selectHasElement = function(new_element) {
    for(var i = 0; i < this.length; i++) {
       var register = this[i];                 
       if(register.status == new_element.status && register.desc_status == new_element.desc_status) {
           return true;
       }
    }
    
    return false;
};

Array.prototype.clear = function() {
    var inner_array = this;   
    inner_array = [];
};


