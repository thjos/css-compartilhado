var TABLE_TOKENS_ACESSO = {
    data: [
        {
            id: 367,
            token: "iJkgHIGW4JrgYrjOW88JTSUWbRaX5dfHwuBGwCT9barOb",
            dth_token: "2020-05-05 10:20:10",
            tempo_expiracao: "2020-05-05 12:01:27",
            credencial: 5,
            perfil: 7
        },
        {
            id: 368,
            token: "kekhyXYHDJEikH6p6jEA05dB2fBRV06xneq4AnhCxqfSb",
            dth_token: "2020-05-05 13:21:29",
            tempo_expiracao: "2020-05-05 16:13:46",
            credencial: 5,
            perfil: 7
        }
    ],
    
    findAll: function() {
        var table = JSON.parse(JSON.stringify(TABLE_TOKENS_ACESSO));
        
        if(!table.data) throw new Error("Nenhuma token de acesso encontrado");
        
        return table.data;
    },
    
    findByStudyRegisterId: function(id) {
        var table = JSON.parse(JSON.stringify(TABLE_SUBJECTS));
        
        var data = table.data.filter(function(element) {
            return element.id_registro_estudo == id;
        }); 
        
        if(!data) throw new Error("Nenhuma assunto encontrado com o id: "+ id);
        
        return data;
    }
};


