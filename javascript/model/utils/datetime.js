/***************************************************************************************************************************************************
/* DataTableUtils */
/***************************************************************************************************************************************************/
class DatetimeUtils {
    constructor() {}

    static formatToBrazil(dataMysql) {
        var data = new Date(dataMysql);
        var dia  = data.getDate().toString();
        var diaF = (dia.length == 1) ? '0' + dia : dia;
        var mes  = (data.getMonth() + 1).toString(); //+1 pois no getMonth Janeiro começa com zero.
        var mesF = (mes.length == 1) ? '0' + mes : mes;
        var anoF = data.getFullYear();

        if(diaF && mesF && anoF) {
            return diaF + "/" + mesF + "/" + anoF;
        } else {
            return "";
        }
    }
}

