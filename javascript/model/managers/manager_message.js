class MessageManager {
    static show(id, category, message) {
        $$(id).css("display", "grid");
        $$(id).addClass("message-"+category);
        $$(id + " .message-text").html(message);
        
        window.setTimeout(MessageManager.hide, 3000, id);
    }
    
    static hide(id) {
        $$(id).css("display", "none");
        $$(id).removeClass("message-success");
        $$(id).removeClass("message-error");
        $$(id + " .message-text").html("");
    }
}


