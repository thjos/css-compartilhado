/**************************************************************************************************************************************************
* TechnicalNoteDispatchEndpoint
****************************************************************************************************************************************************/
class TechnicalNoteEndpoint {
    static endpoint = 'http://cgmsisaudpb1.hospedagemdesites.ws/backend/api/v1/technicalnote.php';
    
    /* ********************************************************************************************************
     * Função: searchTechnicalNotes()
     * Descrição: Retorna a lista notas técnicas encontradas no banco de dados.
     * ******************************************************************************************************** */
    static async searchTechnicalNotes(nt_year,nt_number,process_year, process_number,interested,status) {
        try {
            var filters = [];
            
            if(nt_year || nt_number || process_year || process_number || interested || status) {
                var filters = [];
                filters.push({ano_nt: nt_year, numero_nt: nt_number, ano_processo:process_year, numero_processo: process_number, interessada: interested, status: status });
            }

            var token = BrowserManager.getTokenFromBrowser();   
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, ordem: "desc", filtros: filters, acao: '1'});          
            
            if(data.status != "0") throw new Error(data.mensagem);
            
            return data.dados;
        } catch(e) {
            throw e;
        }
    }
    
    /* ********************************************************************************************************
     * Função: searchProcessTechnicalNotes()
     * Descrição: Retorna a lista notas técnicas encontradas no banco de dados.
     * ******************************************************************************************************** */
    static async searchProcessTechnicalNotes(nt_year,nt_number,process_year, process_number,interested,status) {
        try {
            var filters = [];

            if(nt_year || nt_number || process_year || process_number || interested || status) {
                var filters = [];
                filters.push({ano_nt: nt_year, numero_nt: nt_number, ano_processo:process_year, numero_processo: process_number, interessada: interested, status: status });
            }

            var token = BrowserManager.getTokenFromBrowser();   
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, ordem: "desc", filtros: filters, acao: '6'});          
            
            if(data.status != "0") throw new Error(data.mensagem);
            
            return data.dados;
        } catch(e) {
            throw e;
        }
    }

    /* ********************************************************************************************************
     * Função: loadTechnicalNotesYears()
     * Descrição: Retorna a lista de anos das notas técnicas presentes no banco de dados.
     * ******************************************************************************************************** */
    static async loadTechnicalNotesYears(format_to_select) {
        try {
            var token = BrowserManager.getTokenFromBrowser();   
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, acao: '7'});          
            
            if(data.status != "0") throw new Error(data.mensagem);
            
            if(format_to_select == true) {
                var select_values = [];
                select_values.push({status: '', desc_status: '' });
      
                for(var i = 0; i < data.dados.length; i++) {
                    var ano = data.dados[i].ano;

                    select_values.push({status: ano, desc_status: ano });
                }

                return select_values;
            }
            
            return data.dados;
        } catch(e) {
            throw e;
        }
    }
    
    /* ********************************************************************************************************
     * Função: loadTechnicalNotesNumbers()
     * Descrição: Retorna a lista de números de notas técnicas presentes no banco de dados.
     * ******************************************************************************************************** */
    static async loadTechnicalNotesNumbers(format_to_select) {
        try {
            var token = BrowserManager.getTokenFromBrowser();   
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, acao: '8'});          
            
            if(data.status != "0") throw new Error(data.mensagem);
            
            if(format_to_select == true) {
                var select_values = [];
                select_values.push({status: '', desc_status: '' });
      
                for(var i = 0; i < data.dados.length; i++) {
                    var numero = data.dados[i].numero;

                    select_values.push({status: numero, desc_status: numero });
                }

                return select_values;
            }
            
            return data.dados;
        } catch(e) {
            throw e;
        }
    }
}






