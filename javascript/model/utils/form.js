/*================================================================================================
* Função que impede que os formulários sejam submetidos
*================================================================================================*/
async function preventFormSubmit() {
    var forms = document.querySelectorAll('form');
      
    for (var i = 0; i < forms.length; i++) {
        forms[i].addEventListener('submit', function(event) {
            event.preventDefault();
        });
    }
}

/*================================================================================================
* Preenche o select dropdown baseando-se no id
*================================================================================================*/
function fillDropdown(id, valores, first) {
    var select = document.getElementById(id);
    
    if(!select) return false;

    if(select.options) {
        select.options.length = 0; // clear out existing items    
        
        if(!first) select.options.add(new Option("", ""));
        
        for (var i = 0; i < valores.length; i++) {
            var valor = valores[i];
            select.options.add(new Option(valor[1], valor[0]));
        } 
    }
}


