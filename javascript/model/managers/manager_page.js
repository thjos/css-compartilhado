class PageManager {
    static main = null;
    static current = null;
    
    constructor() { }

    static ignoreToken() {
        var results = new RegExp('[\?&]' + "token" + '=([^&#]*)').exec(window.location.href);
        
        if(results && results[1] == "ignore") {
            return true;
        } else {
            return false;
        }
    }
    
    static showLoader() {
        $$("#app-loader").addClass("show");
        //window.setTimeout(PageManager.hideLoader, 2000);
    }
    
    static hideLoader() {
        $$("#app-loader").removeClass("show");
    } 
    
    /*static showLoader() {
        $$("#app-loader").css("visibility", "visible");
    }
    
    static hideLoader() {
        $$("#app-loader").css("visibility", "hidden");
    } 
    */
    static setVisible(id) {
        $$(id).css("visibility", "visible");
    }
    
    static setInvisible(id) {
        $$(id).css("visibility", "hidden");
    }
    
    static updatePage() {
        PageManager.main.$update();
        PageManager.current.$update();
    }
}


