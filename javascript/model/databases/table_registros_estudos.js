var TABLE_REGISTROS_ESTUDOS = {
    data: [
        { id: 1, register_date: "21/04/2020", update_date: "21/04/2020", ano: '2020', categoria: 'Engenharia de Software', tipo: 'Curso', material: 'Engenharia de Software p/ Senado Federal (Analista Legislativo - Análise de Sistemas) - 2019/2020', link: "https://www.estrategiaconcursos.com.br/app/dashboard/cursos/111094/aulas", init_date: "21/04/2020", end_date: "21/04/2020", status: 3},
        { id: 2, register_date: "21/04/2020", update_date: "21/04/2020", ano: '2020', categoria: 'Direito Administrativo', tipo: 'Curso', material: 'Noções de Direito Administrativo p/ Senado Federal (Analista Legislativo - Diversas Áreas) 2019/2020', link: "https://www.estrategiaconcursos.com.br/app/dashboard/cursos/111094/aulas", init_date: "21/04/2020", end_date: "21/04/2020", status: 3}
    ],
    findAll: function() {
        var table  = JSON.parse(JSON.stringify(TABLE_REGISTROS_ESTUDOS));

        return table.data;
    },
    findById: function(id) {
        var table = JSON.parse(JSON.stringify(TABLE_REGISTROS_ESTUDOS));
        
        var data = table.data.find(function(element) {
            return element.id == id;
        }); 
        
        if(!data) throw new Error("Nenhum registro de estudo encontrado com o id: " + id);
        
        data.status_list = TABLE_STATUS_REGISTROS_ESTUDOS.findAll();
        data.subjects = TABLE_SUBJECTS.findByStudyRegisterId(id);
        
        return data;
    }
};

