/**************************************************************************************************************************************************
* Objeto: EvaluationCycleEndpoint
****************************************************************************************************************************************************/
class StudyRegisterEndpoint {
    static endpoint = 'http://cgmsisaudpb1.hospedagemdesites.ws/backend/api/v1/study_registers.php';
    
    static async searchStudyRegisters() {
        try {  
            var study_registers = TABLE_REGISTROS_ESTUDOS.findAll();
            var status_list     = TABLE_STATUS_REGISTROS_ESTUDOS.findAll();
        
            for(var i = 0; i < study_registers.length; i++) {
                study_registers[i].status_list = status_list;
            }

            return study_registers;           
        } catch(e) {
            LoggingEndpoint.logMessage(e);
            throw e;
        }
    }
    
    static async searchStudyRegisterById(id) {
        try { 
            var study_register = TABLE_REGISTROS_ESTUDOS.findById(id);
            
            study_register.subjects.forEach(function(item, index) {
                item.state = '';
            });

            return study_register;       
        } catch(e) {
            LoggingEndpoint.logMessage(e);
            throw e;
        }
    }
    
    
    static async searchStatusStudyRegisters() {
        try {  
            var status = TABLE_STATUS_REGISTROS_ESTUDOS.findAll();

            return status;           
        } catch(e) {
            LoggingEndpoint.logMessage(e);
            throw e;
        }
    }
}






