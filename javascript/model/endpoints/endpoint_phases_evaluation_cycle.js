/**************************************************************************************************************************************************
* Objeto: EvaluationCycleEndpoint
****************************************************************************************************************************************************/
class PhasesEvaluationCycleEndpoint {
    static endpoint = 'http://cgmsisaudpb1.hospedagemdesites.ws/backend/api/v1/phases_evaluation_cycle.php';

    static async update(id, cycle, phase, status, nu_days, init_day, end_date) {
        try {
            var token = BrowserManager.getTokenFromBrowser();

            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { 
                token: token.token, 
                cargo: token.cargo, 
                funcao: token.funcao, 
                acao: "3",
                id: id,
                ciclo_avaliacao: cycle,
                fase: phase,
                status: status,
                dias_nao_uteis: nu_days,
                data_inicio: init_day, 
                data_fim: end_date
            });
            
            if(data.status != 0) throw new Error("Houve um problema na atualização da fase do ciclo de avaliação");
            
            return data.dados;
        } catch(e) {
            throw e;
        }
    }
}






