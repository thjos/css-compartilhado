/**************************************************************************************************************************************************
* TechnicalNoteDispatchEndpoint
****************************************************************************************************************************************************/
class TechnicalNoteDispatchStatusEndpoint {
    static endpoint = 'http://cgmsisaudpb1.hospedagemdesites.ws/backend/api/v1/technicalnote_dispatch_status.php';
    
    static async searchTechnicalNoteDispatchStatus(format_to_select) {
        try {
            var filters = [];

            /*if(year || quarter || status || init_date || end_date) {
                var filters = [];
                filters.push({ ano: year, trimestre:quarter, status: status, data_inicio_periodo_avaliativo: init_date, data_fim_periodo_avaliativo: end_date });
            }*/

            var token = BrowserManager.getTokenFromBrowser();   
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, ordem: "desc", filtros: filters, acao: '1'});          
            
            if(data.status != "0") throw new Error(data.mensagem);
            
            if(format_to_select == true) {
                var select_values = [];
                select_values.push({status: '', desc_status: '' });
      
                for(var i = 0; i < data.dados.length; i++) {
                    var id = data.dados[i].id;
                    var status = data.dados[i].status;

                    select_values.push({status: id, desc_status: status });
                }

                return select_values;
            }
            
            return data.dados;
        } catch(e) {
            throw e;
        }
    }
}






