/**************************************************************************************************************************************************
* Objeto: LoggingEndpoint
* Descrição: 
* Métodos:
****************************************************************************************************************************************************/
class LoggingEndpoint {
    static endpoint = 'http://cgmsisaudpb1.hospedagemdesites.ws/backend/api/v1/logging.php';

    static async logMessage(message) {
        try {  
            if(!message) return;
            
            var results = new RegExp('[\?&]' + "mode" + '=([^&#]*)').exec(window.location.href);
        
            if(results && results[1] == "debug") {
                console.log("Log:" + message.message);
            }
           
            var data = await AjaxManager.callServer(this.endpoint, "json", 'POST', {data: "", arquivo: "", metodo: "", mensagem: message, acao: '2'});
            
            return data.dados;
        } catch(e) {
            LogManager.log(e);
        }
    }
}
