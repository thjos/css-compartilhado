var TABLE_STATUS_REGISTROS_ESTUDOS = {
    data: [
        { status: "1", desc_status: 'Finalizado'},
        { status: "2", desc_status: 'Em execução'},
        { status: "3", desc_status: 'A iniciar'}
    ],
    findAll: function() {
        var table = JSON.parse(JSON.stringify(TABLE_STATUS_REGISTROS_ESTUDOS));
        
        if(!table.data) throw new Error("Nenhuma status de registro de estudo encontrado");
        
        return table.data;
    },
};


