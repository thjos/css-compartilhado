//https://thisinterestsme.com/handle-ajax-error-jquery/
class AjaxManager {   
    static async callServer(url, dataType, type, data) {
        try {
            var apiOutput = await $.ajax({url: url, dataType: dataType, type: type, data: data});
            
            //console.log(apiOutput);
            
            if(apiOutput.status == "1001") {
                throw new Error("O sistema está apresentando instabilidade no acesso ao banco de dados. Por favor, tente mais tarde.");
            }
            
            return apiOutput;
        } catch(e) {
            throw e;
        }  
    }
    
    static async callServerSync(url, dataType, type, data) {
        try {
            jQuery.ajaxSetup({async:false});
            
            var apiOutput = await $.ajax({url: url, dataType: dataType, type: type, data: data});
            
            if(apiOutput.status == "1001") {
                throw new Error("O sistema está apresentando instabilidade no acesso ao banco de dados. Por favor, tente mais tarde.");
            }
            
            return JSON.parse(apiOutput);
        } catch(e) {
            throw e;
            //LoggingEndpoint.logMessage(e.responseText);
        }  
    }
    
    static ignoreAPI() {
        var results = new RegExp('[\?&]' + "api" + '=([^&#]*)').exec(window.location.href);
        
        if(results && results[1] == "ignore") {
            return true;
        } else {
            return false;
        }
    }
}


