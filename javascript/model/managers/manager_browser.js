class BrowserManager {
    static getTokenFromBrowser() {  
        var token = window.localStorage.getItem('token'); 
        
        if(token) {
            return JSON.parse(token); 
        } else {
            return {ip_acesso:"000.000.000.000",token:"",cargo:"",funcao:"",dth_token:""};
        }
    }

    static setTokenInBrowser(token) {   
        window.localStorage.setItem('token', JSON.stringify(token));
    }
    
    static clearTokenInBrowser() {   
        window.localStorage.removeItem('token');
    }   
}


