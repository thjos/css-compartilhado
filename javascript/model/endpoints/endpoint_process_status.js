/**************************************************************************************************************************************************
* TechnicalNoteDispatchEndpoint
****************************************************************************************************************************************************/
class ProcessStatusEndpoint {
    static endpoint = 'http://cgmsisaudpb1.hospedagemdesites.ws/backend/api/v1/process_status.php';
    
    static async searchProcessStatus(format_to_select) {
        try {
            var filters = [];

            /*if(year || quarter || status || init_date || end_date) {
                var filters = [];
                filters.push({ ano: year, trimestre:quarter, status: status, data_inicio_periodo_avaliativo: init_date, data_fim_periodo_avaliativo: end_date });
            }*/

            var token = BrowserManager.getTokenFromBrowser();   
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, ordem: "desc", filtros: filters, acao: '1'});          
            
            if(data.status != "0") throw new Error(data.mensagem);
            
            if(format_to_select == true) {
                var select_values = [];
                select_values.push({status: '', desc_status: '' });
      
                for(var i = 0; i < data.dados.length; i++) {
                    var id = data.dados[i].id;
                    var status = data.dados[i].status;

                    select_values.push({status: id, desc_status: status });
                }

                return select_values;
            }
            
            return data.dados;
        } catch(e) {
            throw e;
        }
    }
    
    static async searchEvaluationCycleById(id) {
        /*try {
            var token = BrowserManager.getTokenFromBrowser();
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, id_ciclo: id, acao: '2'});
          
            data.dados.status_list = TABLE_STATUS_CICLOS_AVALIACAO.findAll();
          
            if(data.status != "0") throw new Error(data.mensagem);

            return data.dados;
        } catch(e) {
            throw e;
        }*/
    }
    
    static async updateEvaluationCycleStatus(id, status) {
        /*try {
            var token = BrowserManager.getTokenFromBrowser();
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, ordem: "asc", id_ciclo: id,status_ciclo:status, acao: '6'});
            
            if(data.status != "0") throw new Error(data.mensagem);
            
            return data.dados;
        } catch(e) {
            throw e;
        }*/
    }
    
    static async insertEvaluationCycle(ano, trimestre) {
        /*try {
            var token = BrowserManager.getTokenFromBrowser();
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, ano: ano,trimestre:trimestre, acao: '4'});

            if(data.status != "0") throw new Error(data.mensagem);
            
            return data;
        } catch(e) {
            throw e;
        }*/
    }
    
    static async deleteEvaluationCycle(id) {
        /*try {
            var token = BrowserManager.getTokenFromBrowser();
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, id: id, acao: '5'});
            
            return data;
        } catch(e) {
            throw e;
        }*/
    }
    
    static async loadYears(evaluation_cycles) {
        /*try {
            var anos = [];
            anos.push('');

            for(var i = 0; i < evaluation_cycles.length; i++) {
                var ano = evaluation_cycles[i].ano;
                
                if(!anos.includes(ano)) { 
                    anos.push(ano); 
                }
            }
            
            return anos;
        } catch(e) {
            throw e;
        }*/
    }
    
    static async loadQuarters(evaluation_cycles) {
        /*try {
            var trimestres = [];
            trimestres.push('');

            for(var i = 0; i < evaluation_cycles.length; i++) {
                var trimestre = evaluation_cycles[i].trimestre;
                
                if(!trimestres.includes(trimestre)) { 
                    trimestres.push(trimestre); 
                }
            }
            
            return trimestres;           
        } catch(e) {
            throw e;
        }*/
    }
    
    static async loadStatus(evaluation_cycles) {
        /*try {
            var status_list = [];
            var status_inserted = [];

            status_list.push({status: '', desc_status: ''});

            for(var i = 0; i < evaluation_cycles.length; i++) {
                var status      = evaluation_cycles[i].status;
                var desc_status = evaluation_cycles[i].desc_status;

                if(!status_inserted.includes(status)) {
                    status_inserted.push(status);
                    status_list.push({status: status, desc_status: desc_status});
                }   
            }

            return status_list;
        } catch(e) {
            throw e;
        }*/
    }
    
    static async loadReportTypesfunction() {
        /*try {
            var report_types = [];

            report_types.push({id: 1, type: 'pdf'}); 
            report_types.push({id: 2, type: 'xlsx'});
            report_types.push({id: 3, type: 'csv'});

            return report_types;
        } catch(e) {
            throw e;
        }*/
    }  
    
    static async loadSecretarys(indicadores_metas) {
        /*try {
            var secretarias_list = [];
            var secretaria_inserted = [];


            for(var i = 0; i < indicadores_metas.length; i++) {
                var id_secretaria    = indicadores_metas[i].id_secretaria;
                var sigla_secretaria = indicadores_metas[i].sigla_secretaria;

                if(!secretaria_inserted.includes(id_secretaria)) {
                    secretaria_inserted.push(id_secretaria);
                    secretarias_list.push({id_secretaria: id_secretaria, sigla_secretaria: sigla_secretaria});
                }   
            }

            return secretarias_list;
        } catch(e) {
            throw e;
        }*/
    }
}






