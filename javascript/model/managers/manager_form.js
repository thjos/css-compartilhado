class Field {
    id = null;
    value = null;
    type  = null;
    
    constructor(id, value, type) {
        this.id = id ? id : "";
        
        if(value) {
            if(!Array.isArray(value)) {
                this.value = value.trim();
            } else {
                this.value = value;
            }
        } else {
            this.value = "";
        }

        this.type = type ? type : "";
    }
    
    getId() {
        return this.id;
    }
    
    isEmpty() {
        if(this.type === "text" || this.type === "password") {   
            if(this.value == "" || this.value == null) {                
                return true;
            }
        }
        
        if(this.type === "select") {  
            if(this.value == 0 || this.value == "0" || this.value == null) { 
                return true;
            }
        }
        
        if(this.type === "date") {
            if(!(this.value[2] && this.value[1] && this.value[0])) {                   
                return true;
            }
        }
        
        return false;
    }
    
    getValue() {
        if(this.type === "date") {
            return [this.value[2], this.value[1], this.value[0]].join('/');
        } else {
            return this.value;
        }       
    }
    
    getType() {
        return this.type;
    }
    
    focus() {
        $(this.id).focus();
    }
    
    validateEmpty(message) {
        if(this.type === "text" || this.type === "password") {   
            if(this.value == "" || this.value == null) {
                this.focus();
                throw new Error(message);                   
                return "";
            }
        }
        
        if(this.type === "select") {  
            if(this.value == 0 || this.value == "0" || this.value == null) { 
                this.focus();
                throw new Error(message); 
                return "";
            }
        }
        
        if(this.type === "date") {
            if(!(this.value[2] && this.value[1] && this.value[0])) {
                this.focus();
                throw new Error(message);                     
                return "";
            }
        }
        
        return this;
    }
    
    validateNumeric(message) {
        if(isNaN(this.value)) {
            this.focus();
            throw new Error(message); 
            return "";
        }
        
        return this;
    }
}

class FormManager {
    static TYPE_SELECT = "select";
    static TYPE_TEXT = "text";
    static TYPE_PASSWORD = "password";
    static TYPE_DATE = "date";
    static TYPE_SPAN = "span";
    static fieldId = "";
    static fieldType = "";
    static validation = false;
    static is_numeric = false;
    static is_numeric_message = "";
    static validation_message = "";
    
    constructor() {}
       
    static field(id, type) {
        this.validation = false;
        this.validation_message = "";
        this.fieldId   = id;
        this.fieldType = type;
        return this;
    }
    
    static validate(validation, message) {
        this.validation = validation;
        this.validation_message = message;
        return this;
    }
    
    static testValidation() {
        if(this.validation == true) { 
            this.focus();
            throw new Error(this.validation_message); 
        } 
    }
    
    static isNumeric(is_numeric, message) {
        this.is_numeric = is_numeric;
        this.is_numeric_message = message;
        return this;
    }
    
    static testIsNumeric() {
        if(this.is_numeric == true) { 
            this.focus();
            throw new Error(this.is_numeric_message); 
        } 
    }
    
    static getSelect(return_field) {
        var data = $(this.fieldId).val();

        if(return_field == true) {
            return new Field(this.fieldId, data, FormManager.TYPE_SELECT);
        }

        if(data == 0 || data == "0" || data == null) { 
            this.testValidation(); 
            return "";
        }  

        return data;
    }
    
    static getTextPassword(return_field) {
        var data = $(this.fieldId).val();
        
        if(return_field == true) {
            return new Field(this.fieldId, data, FormManager.TYPE_TEXT);
        }
        
        if(data == "" || data == null) {
            this.testValidation();                     
            return "";
        }

        return data;
    }
    
    static getDate(return_field) {
        var date = $(this.fieldId).val().split("-");
        
        if(return_field == true) {
            return new Field(this.fieldId, date, FormManager.TYPE_DATE);
        }
               
        if(!(date[2] && date[1] && date[0])) {
            this.testValidation();                    
            return "";
        }

        return [date[2], date[1], date[0]].join('/');
    }
      
    static get(return_field) {     
        if(this.fieldType === FormManager.TYPE_SELECT) {            
            return this.getSelect(return_field);  
        }
        
        if(this.fieldType === FormManager.TYPE_TEXT || this.fieldType === FormManager.TYPE_PASSWORD) {            
            return this.getTextPassword(return_field);
        }
        
        if(this.fieldType === FormManager.TYPE_DATE) {
            return this.getDate(return_field);
        }
    }
    
    //Esta função tem a responsabilidade de formatar os dados para que eles possa sel colocados em um campo select do formulário 
    static formatToSelect(dados, id_dados, nome_dados) {
        var select_values = [];
        select_values.push({status: ' ', desc_status: ' '});

        for(var i = 0; i < dados.length; i++) {
            var id   = dados[i][id_dados];
            var nome = dados[i][nome_dados];

            select_values.push({status: id, desc_status: nome });
        }

        return select_values; 
    }
        
    static set(values) {    
        if(this.fieldType === FormManager.TYPE_TEXT || this.fieldType === FormManager.TYPE_PASSWORD) {
            $(this.fieldId).val(values);
        }
        
        if(this.fieldType === FormManager.TYPE_SELECT) {
            var select = $(this.fieldId);
            var selected_value = $(this.fieldId).val();
            
            select.empty();

            for(var i = 0; i < values.length; i++) {
                if(selected_value == values[i].status) {
                    select.append($("<option selected />").val(values[i].status).text(values[i].desc_status));
                } else {
                    select.append($("<option />").val(values[i].status).text(values[i].desc_status));
                }
            }
        }
        
        if(this.fieldType === FormManager.TYPE_SPAN) {
            $(this.fieldId).html(values);
        }
        
        if(this.fieldType === FormManager.TYPE_DATE) {
            var date = values.split("/");
            
            date = [date[2], date[1], date[0]].join('-');
            
            $(this.fieldId).val(date);
        }
        
        return this;
    }

    static reset() {
        if(this.fieldType === FormManager.TYPE_SELECT) {
            $(this.fieldId).prop('selectedIndex',0);
        }
        
        if(this.fieldType === FormManager.TYPE_TEXT) {
            $(this.fieldId).val('');
        }
        
        if(this.fieldType === FormManager.TYPE_PASSWORD) {
            $(this.fieldId).val('');
        }
        
        if(this.fieldType === FormManager.TYPE_DATE) {
            $(this.fieldId).val('');
        } 
        
        return this;
    }
    
    static focus() {
        $(this.fieldId).focus();
        return this;
    }
    
    static select(value) {
        var self = this;
        
        if(this.fieldType === FormManager.TYPE_SELECT) {
            $(self.fieldId+" option[value='"+value+"']").prop('selected',true);
            PageManager.current.$update();
        }
        
        return this;
    }
    
    static click(callback) {
        $(this.fieldId).click(callback);
        
        return this;
    }
} 


