/**************************************************************************************************************************************************
* Objeto: EvaluationCycleEndpoint
****************************************************************************************************************************************************/
class CycleGoalIndicatorEndpoint {
    static endpoint = 'http://cgmsisaudpb1.hospedagemdesites.ws/backend/api/v1/evaluation_cycle_institutional_goal_indicator.php';

    static async update(id, ciclo, indicador_meta, meta, realizado, ordem) {
        try {
            var token = BrowserManager.getTokenFromBrowser();

            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { 
                token: token.token, 
                cargo: token.cargo, 
                funcao: token.funcao,  
                id: id,
                ciclo: ciclo,
                indicador_meta_institucional: indicador_meta,
                meta: meta,
                realizado: realizado,
                ordem: ordem,
                acao: '3'
            });
            
            if(data.status != 0) throw new Error("Houve um problema na atualização do indicador");
            
            return data.dados; 
        } catch(e) {
            throw e;
        }
    }
}






