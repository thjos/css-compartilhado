/**************************************************************************************************************************************************
* LoginEndpoint
* Descrição: Objeto relacionado ao endpoint responsável pelas funções de login de acesso.
* Métodos:
* - listActiveProfiles: Valida o login e senha e lista os perfis ativos para o funcionário.
* - createAccessToken: Método que cria o token de acesso para o funcionário.
* - logout: Método invalida o token de acesso do funcionário.
****************************************************************************************************************************************************/
class LoginEndpoint {
    static endpoint = 'http://cgmsisaudpb1.hospedagemdesites.ws/backend/api/v1/login.php';

    static async listActiveProfiles(registration_number, password) {
        try {    
            var data = await AjaxManager.callServer(this.endpoint, "json", 'POST', { matricula: registration_number, senha: password, acao: '1'});
            
            if(data.status != 0) throw new Error(data.mensagem);
            
            return data.dados;
        } catch(e) {
            throw e;
        }
    }
    
    static async createAccessToken(registration_number, password, profile) {
        try { 
            var data = await AjaxManager.callServer(this.endpoint, "json", 'POST', { matricula: registration_number, senha: password, perfil: profile, acao: '2'}); 
            return data.dados;
        } catch(e) {
            throw e;
        }
    }
    
    static async logout(token) {
        try { 
            var data = await AjaxManager.callServer(this.endpoint, "json", 'POST', {token: token.token, cargo: token.cargo, funcao: token.funcao, acao: '3'}); 
            return data.dados;
        } catch(e) {
            throw e;
        }
    }
}
