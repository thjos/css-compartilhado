/**************************************************************************************************************************************************
* ProcessEndpoint
****************************************************************************************************************************************************/
class ProcessEndpoint {
    static endpoint = 'http://cgmsisaudpb1.hospedagemdesites.ws/backend/api/v1/process.php';
    
    /* ********************************************************************************************************
     * Função: searchProcess()
     * Descrição: Busca a lista de processos no banco de dados
     * ******************************************************************************************************** */
    static async searchProcess(year, number, origem, status) {
        try {
            var filters = [];
            
            if(year != "" || number != "" || origem != "" || status != "") {
                filters.push({ ano: year, numero: number, origem: origem, status: status });
            }

            var token = BrowserManager.getTokenFromBrowser();   
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, ordem: "desc", filtros: filters, acao: '1'});          
            
            if(data.status != "0") throw new Error(data.mensagem);
            
            return data.dados;
        } catch(e) {
            throw e;
        }
    }
    
    /* ********************************************************************************************************
     * Função: deleteProcess()
     * Descrição: Insere um novo processo no banco de dados
     * ******************************************************************************************************** */
    static async insertProcess(data_entrada, origem, ano, numero, area, tipo, assunto, nivel_complexidade, nivel_urgencia, covid, pontos, servidores_envolvidos) {
        try {
            var token = BrowserManager.getTokenFromBrowser();   
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { 
                token: token.token, 
                cargo: token.cargo, 
                funcao: token.funcao, 
                data_entrada: data_entrada, 
                origem: origem, 
                ano: ano,
                numero: numero,
                area: area,
                tipo: tipo,
                assunto: assunto, 
                nivel_complexidade: nivel_complexidade,
                nivel_urgencia: nivel_urgencia,
                covid: covid,
                pontos:pontos,
                servidores_envolvidos: servidores_envolvidos,
                acao: '4'
            });          
            
            if(data.status != "0") throw new Error(data.mensagem);
            
            return data;
        } catch(e) {
            throw e;
        }
    }
    
    /* ********************************************************************************************************
     * Função: deleteProcess()
     * Descrição: Apaga um processo no banco de dados pelo seu ID 
     * ******************************************************************************************************** */
    static async deleteProcess(id) {
        try {
            var token = BrowserManager.getTokenFromBrowser();
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, id: id, acao: '5'});
            
            return data;
        } catch(e) {
            throw e;
        }
    }
    
    /* ********************************************************************************************************
     * Função: searchProcessById()
     * Descrição: Busca um processo por seu id
     * ******************************************************************************************************** */
    static async searchProcessById(id) {
        try {
            var token = BrowserManager.getTokenFromBrowser();
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, id_processo: id, acao: '2'});

            if(data.status != "0") throw new Error(data.mensagem);

            return data.dados;
        } catch(e) {
            throw e;
        }
    }

    /* ********************************************************************************************************
     * Função: loadProcessYears()
     * Descrição: Retorna a lista de anos dos processo presentes no banco de dados.
     * ******************************************************************************************************** */
    static async loadProcessYears(format_to_select) {
        try {
            var token = BrowserManager.getTokenFromBrowser();   
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, acao: '6'});          
            
            if(data.status != "0") throw new Error(data.mensagem);
            
            if(format_to_select == true) {
                var select_values = [];
                select_values.push({status: '', desc_status: '' });
      
                for(var i = 0; i < data.dados.length; i++) {
                    var ano = data.dados[i].ano;

                    select_values.push({status: ano, desc_status: ano });
                }

                return select_values;
            }
            
            return data.dados;
        } catch(e) {
            throw e;
        }
    }
    
    /* ********************************************************************************************************
     * Função: loadProcessNumbers()
     * Descrição: Retorna a lista de números dos processos presentes no banco de dados.
     * ******************************************************************************************************** */
    static async loadProcessNumbers(format_to_select) {
        try {
            var token = BrowserManager.getTokenFromBrowser();   
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'POST', { token: token.token, cargo: token.cargo, funcao: token.funcao, acao: '7'});          
            
            if(data.status != "0") throw new Error(data.mensagem);
            
            if(format_to_select == true) {
                var select_values = [];
                select_values.push({status: '', desc_status: '' });
      
                for(var i = 0; i < data.dados.length; i++) {
                    var numero = data.dados[i].numero;

                    select_values.push({status: numero, desc_status: numero });
                }

                return select_values;
            }
            
            return data.dados;
        } catch(e) {
            throw e;
        }
    }

    static async loadReportTypesfunction() {
        /*try {
            var report_types = [];

            report_types.push({id: 1, type: 'pdf'}); 
            report_types.push({id: 2, type: 'xlsx'});
            report_types.push({id: 3, type: 'csv'});

            return report_types;
        } catch(e) {
            throw e;
        }*/
    }  
    
    static async loadSecretarys(indicadores_metas) {
        /*try {
            var secretarias_list = [];
            var secretaria_inserted = [];


            for(var i = 0; i < indicadores_metas.length; i++) {
                var id_secretaria    = indicadores_metas[i].id_secretaria;
                var sigla_secretaria = indicadores_metas[i].sigla_secretaria;

                if(!secretaria_inserted.includes(id_secretaria)) {
                    secretaria_inserted.push(id_secretaria);
                    secretarias_list.push({id_secretaria: id_secretaria, sigla_secretaria: sigla_secretaria});
                }   
            }

            return secretarias_list;
        } catch(e) {
            throw e;
        }*/
    }
}






