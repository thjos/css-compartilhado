var TABLE_STATUS_CICLOS_AVALIACAO = {
    data: [
        { status: "1", desc_status: 'Finalizado'},
        { status: "2", desc_status: 'Pendente'},
        { status: "3", desc_status: 'Em execução'},
        { status: "4", desc_status: 'A iniciar'}
    ],
    findAll: function() {
        var table = JSON.parse(JSON.stringify(TABLE_STATUS_CICLOS_AVALIACAO));
        
        if(!table.data) throw new Error("Nenhuma status de ciclo de avaliação encontrado");
        
        return table.data;
    },
};



