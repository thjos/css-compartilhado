class AlertManager { 
    static opened = false;
    
    constructor() {}

    static alertIsOpened() {
        return this.opened == true;
    }
    
    static alertIsClosed() {
        return this.opened == false;
    }

    static showAlert(message) {
        this.opened = true;
        $$("#app-alert").css("visibility", "visible");
        $$("#app-alert").html(message);
        
        window.setTimeout(AlertManager.hideAlert, 2000);
    }
    
    static hideAlert() {
        this.opened = false;
        $$("#app-alert").css("visibility", "hidden");
        $$("#app-alert").html("");
    }
}

