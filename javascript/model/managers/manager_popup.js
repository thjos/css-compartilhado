class PopupManager { 
    static opened = false;
    static data = {};
    
    constructor() {}

    static popupIsOpened() {
        return this.opened == true;
    }
    
    static popupIsClosed() {
        return this.opened == false;
    }
    
    static open(id, width, margin_top) {
        this.opened = true;
        
        $(id).css("display", "block");
        $(id + " .modal-content").css("width", width);
        
        if(margin_top) {
            $(id + " .modal-content").css("margin-top", margin_top);
        }
        
        $(id + " .modal-content .modal-title .title-close").click(function() { PopupManager.close(id); });
    }

    static close(id) {
        if(this.opened == true) { 
            this.opened = false;
            $(id).css("display", "none");
        }
    }
}


