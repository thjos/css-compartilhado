/**************************************************************************************************************************************************
* TechnicalNoteDispatchEndpoint
****************************************************************************************************************************************************/
class RolesFunctionsEndpoint {
    static endpoint = 'http://cgmsisaudpb1.hospedagemdesites.ws/backend/api/v1/roles_functions.php';
    
    //Esta função busca no backend a lista de cargos cadastrados no banco de dados
    static async searchRoles() {
        try {
            var filters = [];

            var token = BrowserManager.getTokenFromBrowser();   
            var data  = await AjaxManager.callServer(this.endpoint, "json", 'GET', { token: token.token, cargo: token.cargo, funcao: token.funcao, ordem: "desc", filtros: filters, acao: '1'});          
            
            if(data.status != "0") throw new Error(data.mensagem);

            return data.dados;
        } catch(e) {
            throw e;
        }
    }
}






