class SnackbarManager { 
    static opened = false;
    
    constructor() {}

    static snackbarIsOpened() {
        return this.opened == true;
    }
    
    static snackbarIsClosed() {
        return this.opened == false;
    }

    static showSnackbar(message) {
        this.opened = true;
        $$("#app-snackbar").addClass("show");
        $$("#app-snackbar").html(message);
        
        window.setTimeout(SnackbarManager.hideSnackbar, 2000);
    }
    
    static hideSnackbar() {
        this.opened = false;
        $$("#app-snackbar").removeClass("show");
        $$("#app-snackbar").html("");
    }
}

