/**************************************************************************************************************************************************
* TokenEndpoint
* Descrição: Objeto relacionado ao endpoint responsável pelas funções de manipulação de tokens de acesso.
* Métodos:
* - validateToken: Método que consulta o token de acesso no endpoint da API, retornando se este está válido ou não.
****************************************************************************************************************************************************/
class TokenEndpoint {
    static endpoint = 'http://cgmsisaudpb1.hospedagemdesites.ws/backend/api/v1/token.php';

    static async validateToken(token) {
        try { 
            var data = await AjaxManager.callServer(this.endpoint, "json", 'POST', {token: token.token, cargo: token.cargo, funcao: token.funcao, acao: '1'});

            if(data.status == "2002" || data.status == "2001") {
                throw new Error(data.mensagem);
            }

            return data.dados;
        } catch(e) {
            throw e;
        }
    }
}
